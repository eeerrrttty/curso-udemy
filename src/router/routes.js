import AuthGuard from './auth-guard'

export default store => {
  const routes = [
    {
      path: '/',
      beforeEnter: (to, from, next) => AuthGuard(to, from, next, store),
      component: () => import('layouts/MainLayout.vue'),
      children: [
        { path: '', component: () => import('pages/Index.vue') }
      ]
    },
    {
      name: 'Login',
      path: '/login',
      component: () => import('pages/Login.vue')
    },
    {
      name: 'Cadastro',
      path: '/cadastro',
      component: () => import('pages/Cadastro.vue')
    },
    {
      name: 'Recuperar',
      path: '/recuperar',
      component: () => import('pages/Recuperar.vue')
    },
    // Always leave this as last one,
    // but you can also remove it
    {
      path: '*',
      component: () => import('pages/Error404.vue')
    }
  ]
  // Always leave this as last one
  if (process.env.MODE !== 'ssr') {
    routes.push({
      path: '*',
      component: () => import('pages/Error404.vue')
    })
  }
  return routes
}
