export const updateTest = (state, payload) => {
  state.test = payload
}
export const setLoading = (state, payload) => {
  state.loading = payload
}
export const setUser = (state, payload) => {
  state.user = payload
}
