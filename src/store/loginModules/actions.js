// import * as firebase from 'firebase'
import { Notify } from 'quasar'

export function cadastro ({ commit, dispatch, getters, router }, payload) {
  console.log(payload)
  commit('setLoading', true)
  // chamar função do firebase --> ativar cloud function.
  // função será processada no servidor firebase
  // obteremos a resposta aqui.
  const creatingUser = this._vm.$firebase.functions().httpsCallable('creatingUser')
  creatingUser({ email: payload.email, senha: payload.senha }).then(function (result) {
    // Read result of the Cloud Function.
    console.log('result', result)
    if (result.data.sucess === true) {
      const user = {
        email: payload.email,
        senha: payload.senha
      }
      commit('setUser', user)
      Notify.create({
        message: 'Cadastro feito com sucesso',
        color: 'green',
        timeout: 2000
      })
    }
    console.log('sucess ?', result.data.sucess)
    if (result.data.sucess === false) {
      console.log('entrando no erro 1')
      console.log(result.data.error.errorInfo.code)
      if (result.data.error.errorInfo.code === 'auth/email-already-exists') {
        console.log('entrando no erro 2')
        Notify.create({
          message: 'Erro: Este e-mail já está em uso, tente com outro.',
          color: 'red',
          timeout: 2000
        })
      }
      // erro 2
      // erro 3
    }
    commit('setLoading', false)
  })
  // this._vm.$firebase.auth().createUserWithEmailAndPassword(payload.email, payload.senha).then(() => {
  //   console.log('cadastrado com sucesso')
  //   commit('setLoading', false)
  //   // salvar usuário no firestore.
  // }).catch(function (error) {
  //   console.log(error)
  //   commit('setLoading', false)
  // })
}

export function login ({ commit, dispatch, getters, router }, payload) {
  console.log(payload)
  // INICIAR LOADING
  commit('setLoading', true)
  this._vm.$firebase.auth().signInWithEmailAndPassword(payload.email, payload.senha).then(() => {
    console.log('login feito com sucesso')
    const user = {
      email: payload.email,
      senha: payload.senha
    }
    commit('setUser', user)
    Notify.create({
      message: 'Login feito com sucesso',
      color: 'green',
      timeout: 2000
    })
    commit('setLoading', false)
    // ENCERRAR LOADING
  }).catch(function (error) {
    console.log(error)
    if (error.code === 'auth/wrong-password') {
      Notify.create({
        message: 'Senha incorreta',
        color: 'red',
        timeout: 2000
      })
    }
    if (error.code === 'auth/user-not-found') {
      Notify.create({
        message: 'Usuário inexistente, confira o e-mail usado',
        color: 'red',
        timeout: 2000
      })
    }
    commit('setLoading', false)
    // ENCERRAR LOADING
  })
}

export function logout ({ commit, dispatch, getters, router }, payload) {
  commit('setUser', null)
  // ENCERRAR LOADING)
}
