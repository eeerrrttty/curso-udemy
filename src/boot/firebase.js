// import something here
import * as firebase from 'firebase'
// leave the export, even if you don't use it
// Use your configuration info
firebase.initializeApp({
  apiKey: 'AIzaSyAKCSIOkk2jam5OZCHBLuV17o3jCiREkKY',
  authDomain: 'curso-udemy-ok.firebaseapp.com',
  databaseURL: 'https://curso-udemy-ok.firebaseio.com',
  projectId: 'curso-udemy-ok',
  storageBucket: 'curso-udemy-ok.appspot.com',
  messagingSenderId: '880999308318',
  appId: '1:880999308318:web:968bff6323f9d36655620f',
  measurementId: 'G-PMMBWJXNKJ'
})

export default async ({ store, app, router, Vue }) => {
  // something to do
  // Após logar, rodar funções do store:
  // console.log('firebase running', firebase)
  // someone used this line, but i commented as i didn't see any effect.
  Vue.prototype.$firebase = firebase
}
export { firebase }
