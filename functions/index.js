const functions = require('firebase-functions')
const admin = require('firebase-admin')
// const request = require('request')
// const requestPromise = require('request-promise')

// const cors = require('cors')({origin: true})

admin.initializeApp()
admin.firestore().settings({timestampsInSnapshots: true})
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions

exports.creatingUser = functions.https.onCall((data, context) => {
  functions.logger.log('Hello logs!', data)
  //   valores possíveis de serem salvos no auth.
  //   email: 'user@example.com',
  //   emailVerified: false,
  //   phoneNumber: '+11234567890',
  //   password: 'secretPassword',
  //   displayName: 'John Doe',
  //   photoURL: 'http://www.example.com/12345678/photo.png',
  //   disabled: false
  return admin.auth().createUser({ email: data.email, emailVerified: true, password: data.senha}).then(() => {
    functions.logger.log('cadastrado com sucesso')
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    let autoId = ''
    for (let i = 0; i < 20; i++) {
      autoId += chars.charAt(Math.floor(Math.random() * chars.length))
    }
    admin.firestore().collection('users').doc(autoId).set({ email: data.email, senha: data.senha })
    functions.logger.log('registrado no firestore com sucesso')
    return { sucess: true }
  }).catch((error) => {
    functions.logger.log(error)
    return { sucess: false, error: error }
    // if (error.code === 'auth/email-already-in-use') {
    // }
  })
})

exports.helloWorld = functions.https.onRequest((request, response) => {
  functions.logger.info('Hello logs!', {structuredData: true})
  response.send('Hello from Firebase!')
})
